@extends('layout/app')

@section('title','Buat Magang')

@section('content')

@foreach( $users as $user)
@if($user->id == auth()->user()->id)

@if (session()->has('success_message'))
    <div class="alert alert-success text-center">
        {{ session()->get('success_message') }}
    </div>
@endif

    <main class="" uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 550; repeat: true">
        <section class="bannerCompany">
            <figure class="bannerCompany__banner">
                <img width="100%" src="/assets/img/kfcbanner.jpg" alt="banner-company">
            </figure>
            <figure class="bannerCompany__profile">
                <img src="/uploads/avatars/{{ $user->avatar }}" alt="profile-photo-company">
            </figure>
            <div class="box-wrap">
                <h1 class="uk-heading-small@s">PT KFC Indonesia</h1>
                <div class="box">
                    <img src="/assets/img/location.svg" alt="location-logo">
                    <p>Sleman City Mall Lantai 3, Jln. Magelang No 45 </p>
                </div>
            </div>
        </section>


        <section>

            <section>
                <div class="slider-profile">
                  <button
                    class="uk-button uk-button-default uk-margin-small-right button-tarik"
                    type="button"
                    uk-toggle="target: #offcanvas-slide"
                  >
                    Tarik
                  </button>
      
                  <div id="offcanvas-slide" uk-offcanvas="overlay: true">
              <div class="uk-offcanvas-bar" style="background: #1e87f0;">
                <button
                  class="uk-offcanvas-close"
                  type="button"
                  uk-close
                ></button>
                <div class="profil__pers uk-text-center">
                  <h2 class="uk-text-bold" style="color: white;">Dashboard</h2>
                  <img
                    class="uk-align-center"
                    src="/assets/img/kfclogo.png"
                    alt=""
                  />
                  <h3 class="uk-text-bold" style="color: white;">
                    KFC INDONESIA
                  </h3>
                  <p class="uk-margin-large-bottom" style="color: white;">
                    0274-4434-1144
                  </p>
                </div>
                <div class="profil__items">
                  <a href="{{ route('perusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon1.png"
                        alt="icon Company Profile"
                      />Company Profile
                    </h4>
                  </a>
                  <a href="profilperusahaan-informasi.html">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon2.png"
                        alt="icon Informasi"
                      />Informasi
                    </h4></a
                  >
                  <a href="{{ route('buatmagangperusahaan.store') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon3.png"
                        alt="icon Buat Magang "
                      />Buat Magang
                    </h4></a
                  >
                  <a href="{{ route('terhubungperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon4.png"
                        alt="icon Terhubung"
                      />Terhubung
                    </h4></a
                  >

                  <a href="{{ route('keamananperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon5.png"
                        alt="icon Keamanan"
                      />Keamanan
                    </h4></a
                  >
                  <a href="{{ route('bantuanperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon6.png"
                        alt="icon Bantuan"
                      />Bantuan
                    </h4></a
                  >

                  <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <h4 class="uk-text-bold" style="color: white; text-align: left;">
                      <img class="uk-margin-small-right" src="/assets/img/icon7.png" alt="" />Exit
                    </h4></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </div>
            </div>

                </div>
              </section>
      


            <div class="perusahaan-cp"> 
                <h1 class=" uk-margin-medium-bottom uk-text-bold ">Buat Magang</h1>
                @foreach( $perusahaan as $peru)
                @foreach( $buatmagang as $magang)
                @if($peru->user_id == auth()->user()->id)
                @if($magang->id_perusahaan == $peru->id)
                    
                    <form class="form-p" action="{{ route('buatmagangperusahaan.update') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                        <p>Nama Magang</p>
                        <input id="jobmagang" name="jobmagang" class="uk-input uk-width-1-1"  type="text" placeholder="ex:Dosen" value="{{ $magang->nama_magang }}">
                        <p>Tentang Magang</p>
                            <div class="uk-margin">
                                <textarea id="tentangmagang" name="tentangmagang"
                                class="uk-textarea textarea-cp"
                                rows="10"
                                placeholder="Masukkan diskripsi perusahaan anda"
                              >{{ $magang->tentang_magang }}</textarea>
                            </div>

                        <p>Masukkan Waktu Kerja</p>
                        <div class="uk-margin waktu-radio uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-radio" type="radio" name="waktukerja" value="Harian" checked> Harian</label>
                            <label><input class="uk-radio" type="radio" name="waktukerja" value="Mingguan"> Mingguan</label>
                            <label><input class="uk-radio" type="radio" name="waktukerja" value="Bulanan"> Bulanan</label>
                            <label><input class="uk-radio" type="radio" name="waktukerja" value="Tahunan"> Tahunan</label>
                        </div>
                        <p>Masukkan Gaji</p>
                        <div class="uk-grid form-gaji">
                            <input class="uk-input uk-width-1-1 gaji-minimal" style="width: 150px" type="text" id="gajimin" name="gajimin" placeholder="Minimal" value="{{$magang->gaji_min}}">

                            <input class="uk-input uk-width-1-1 gaji-maximal" style="width: 150px" type="text" id="gajimax" name="gajimax" placeholder="Maximal" value="{{$magang->gaji_max}}">
                    </div>
                    <p>Di Tempatkan</p>
                    <select class="uk-select crs-country" style="width:150px" data-region-id="negara" name="negara" placeholder=""></select>
                <p>Lokasi</p>
                    <select class="uk-select" style="width:150px" id="negara" name= "kota" placeholder=""></select>

                <p>Tingkat Pendidikan</p>
                <select id="tinkatpendidikan" name="tinkatpendidikan" class="uk-select" style="width:150px" placeholder="">
                    <option>Sekolah Dasar</option>
                    <option value="SMA">Sekolah menengah atas/kejuruan</option>
                    <option value="Diploma Satu(D1)">Diploma Satu(D1)</option>
                    <option value="Diploma Dua(D2)">Diploma Dua(D2)</option>
                    <option value="Diploma Tiga(D3)">Diploma Tiga(D3)</option>
                    <option value="Diploma Empat(D4)">Diploma Empat(D4)</option>
                    <option value="Sarjana(S1)">Sarjana(S1)</option>
                    <option value="Magister(S2)">Magister(S2)</option>
                    <option value="Doktor(S3)">Doktor(S3)</option>

            </select>

            <p>Masukkan Foto Perusahaan</p>
                      
            <p><img id="output" src="/uploads/magang/{{$peru->direktur}}/{{$magang->foto}}" width="140" /></p>
                    <input type="file"  accept="image/*" name="imagemagang" id="file"  onchange="loadFile(event)" style="display: none;">
                    <label class="tombol-upload" for="file" >Upload Image</label>
                    
                    <script>
                    var loadFile = function(event) {
                    var imagemagang = document.getElementById('output');
                    imagemagang.src = URL.createObjectURL(event.target.files[0]);
                    };
                    </script>

            <p>Masukkan password perusahaan</p>
            <input class="uk-input uk-width-1-1" id="password" name="password" style="max-width: 19rem;" type="password" placeholder="Masukkan Password">

                       <button class="uk-button uk-border-rounded uk-button-primary uk-align-center uk-margin-large-top" type="submit">Simpan</button>
                        </form>

                @endif
                @endif
                @endforeach
                @endforeach                
                    </div>
                </div>
                        

                <!-- MODAL BERHASIL -->
                <div id="uploadberhasil" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                                <div class="uk-text-center">
                                <img  src="assets/img/checklist.svg" alt="">
                                <h4>Lamaran anda telah terkirim tunggu konfirmasi lebih lanjut, Kami akan segera mengirim ke email anda</h4>
                                <h4></h4>
                                
                            </div>
        
                            
                    </div>
                </div>
                <!-- END MODAL BERHASIL -->
        </section>


    </main>

@endif
@endforeach
@endsection
    

@section('footer')
    <footer>
        <div class="flex-footer">
            <ul class="ul-list">
                <li class="header"> Perusahaan</li>
                <li><a href="#"> Beranda</a></li>
                <li><a href="#">Tentang Perusahaan</a> </li>
            </ul>
            <ul>
                <li class="header">Bantuan</li>
                <li><a href="#">Hubungi Kami</a> </li>
                <li><a href="#">FAQ</a> </li>
            </ul>
            <ul>
                <li class="header">Produk & Layanan</li>
                <li><a href="#">Lowongan Kerja</a> </li>
                <li><a href="#">Pembelajaran</a> </li>
                <li><a href="#"> Magang</a></li>
            </ul>
            <ul>
                <li class="header">Informasi Lainya</li>
                <li><a href="#">Testimoni</a></li>
            </ul>
        </div>

        <div class="flex-footer-2">
            <ul class="special">
                <li class="header">Temukan kami di</li>
                <div class="social">
                    <a href="#"><img class="social__img--special" src="/assets/img/email-2.png" alt="logo email"></a>
                    <a href="#"><img class="social__img" src="/assets/img/facebook.svg" alt="logo facebook"></a>
                    <a href="#"><img class="social__img" src="/assets/img/twitter.svg" alt="logo twitter"></a>
                </div>
                <li><small>2020 - Lowongan Kerja</small> </li>
                <li><small>&copy All Rights Reserved.</small> </li>
            </ul>
        </div>

    </footer>
    <script src="{!! asset('assets/js/crs.min.js') !!}"></script>
@endsection