@extends('layout/app')

@section('title','Buat Magang')

@section('content')

@foreach( $users as $user)
@if($user->id == auth()->user()->id)

@if (session()->has('success_message'))
    <div class="alert alert-success text-center">
        {{ session()->get('success_message') }}
    </div>
@endif
    
    <main class="" uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 550; repeat: true">
        <section class="bannerCompany">
            <figure class="bannerCompany__banner">
                <img width="100%" src="/assets/img/kfcbanner.jpg" alt="banner-company">
            </figure>
            <figure class="bannerCompany__profile">
                <img src="/uploads/avatars/{{ $user->avatar }}" alt="profile-photo-company">
            </figure>
            <div class="box-wrap">
                <h1 class="uk-heading-small@s">PT KFC Indonesia</h1>
                <div class="box">
                    <img src="/assets/img/location.svg" alt="location-logo">
                    <p>Sleman City Mall Lantai 3, Jln. Magelang No 45 </p>
                </div>
            </div>
        </section>


        <section>

            <section>
                <div class="slider-profile">
                  <button
                    class="uk-button uk-button-default uk-margin-small-right button-tarik"
                    type="button"
                    uk-toggle="target: #offcanvas-slide"
                  >
                    Tarik
                  </button>
      
                  <div id="offcanvas-slide" uk-offcanvas="overlay: true">
              <div class="uk-offcanvas-bar" style="background: #1e87f0;">
                <button
                  class="uk-offcanvas-close"
                  type="button"
                  uk-close
                ></button>
                <div class="profil__pers uk-text-center">
                  <h2 class="uk-text-bold" style="color: white;">Dashboard</h2>
                  <img
                    class="uk-align-center"
                    src="/assets/img/kfclogo.png"
                    alt=""
                  />
                  <h3 class="uk-text-bold" style="color: white;">
                    KFC INDONESIA
                  </h3>
                  <p class="uk-margin-large-bottom" style="color: white;">
                    0274-4434-1144
                  </p>
                </div>
                <div class="profil__items">
                  <a href="{{ route('perusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon1.png"
                        alt="icon Company Profile"
                      />Company Profile
                    </h4>
                  </a>
                  <a href="profilperusahaan-informasi.html">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon2.png"
                        alt="icon Informasi"
                      />Informasi
                    </h4></a
                  >
                  <a href="{{ route('buatmagangperusahaan.store') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon3.png"
                        alt="icon Buat Magang "
                      />Buat Magang
                    </h4></a
                  >
                  <a href="{{ route('terhubungperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon4.png"
                        alt="icon Terhubung"
                      />Terhubung
                    </h4></a
                  >

                  <a href="{{ route('keamananperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon5.png"
                        alt="icon Keamanan"
                      />Keamanan
                    </h4></a
                  >
                  <a href="{{ route('bantuanperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon6.png"
                        alt="icon Bantuan"
                      />Bantuan
                    </h4></a
                  >

                  <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <h4 class="uk-text-bold" style="color: white; text-align: left;">
                      <img class="uk-margin-small-right" src="/assets/img/icon7.png" alt="" />Exit
                    </h4></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </div>
            </div>

                </div>
              </section>
      


            <div class="perusahaan__keamanan uk-padding-large "> 
                <h1 class="uk-margin-medium-bottom uk-text-bold ">Keamanan</h1>
                    
                    <form class="form-p" action="{{ route('keamananperusahaan.store') }}" method="POST">
                      @csrf
                        
                        <p>Password Lama</p>
                        <input id="passwordlama" name="passwordlama" class="uk-input uk-width-1-1" style="max-width: 19rem" type="text" placeholder="Masukkan Password Lama">
                         
                        <p>Password Baru</p>
                        <input id="passwordbaru" name="passwordbaru" class="uk-input uk-width-1-1" style="max-width: 19rem" type="text" placeholder="Masukkan Password Baru">
                         
                        <p>Konfirmasi Password Baru</p>
                        <input id="passwordkonfirm" name="passwordkonfirm" class="uk-input uk-width-1-1" style="max-width: 19rem" type="text" placeholder="Konfirmasi Password">
                        
                          <button class="uk-button uk-border-rounded uk-button-primary uk-align-center uk-margin-large-top">Simpan</button>
                    </form>
                
                    </div>
                </div>
                        

                <!-- MODAL BERHASIL -->
                <div id="uploadberhasil" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                                <div class="uk-text-center">
                                <img  src="/assets/img/checklist.svg" alt="">
                                <h4>Lamaran anda telah terkirim tunggu konfirmasi lebih lanjut, Kami akan segera mengirim ke email anda</h4>
                                <h4></h4>
                                
                            </div>
        
                            
                    </div>
                </div>
                <!-- END MODAL BERHASIL -->
        </section>


    </main>
@endif
@endforeach
@endsection
    

@section('footer')
    <footer>
        <div class="flex-footer">
            <ul class="ul-list">
                <li class="header"> Perusahaan</li>
                <li><a href="#"> Beranda</a></li>
                <li><a href="#">Tentang Perusahaan</a> </li>
            </ul>
            <ul>
                <li class="header">Bantuan</li>
                <li><a href="#">Hubungi Kami</a> </li>
                <li><a href="#">FAQ</a> </li>
            </ul>
            <ul>
                <li class="header">Produk & Layanan</li>
                <li><a href="#">Lowongan Kerja</a> </li>
                <li><a href="#">Pembelajaran</a> </li>
                <li><a href="#"> Magang</a></li>
            </ul>
            <ul>
                <li class="header">Informasi Lainya</li>
                <li><a href="#">Testimoni</a></li>
            </ul>
        </div>

        <div class="flex-footer-2">
            <ul class="special">
                <li class="header">Temukan kami di</li>
                <div class="social">
                    <a href="#"><img class="social__img--special" src="/assets/img/email-2.png" alt="logo email"></a>
                    <a href="#"><img class="social__img" src="/assets/img/facebook.svg" alt="logo facebook"></a>
                    <a href="#"><img class="social__img" src="/assets/img/twitter.svg" alt="logo twitter"></a>
                </div>
                <li><small>2020 - Lowongan Kerja</small> </li>
                <li><small>&copy All Rights Reserved.</small> </li>
            </ul>
        </div>

    </footer>
    <script src="{!! asset('/assets/js/crs.min.js') !!}"></script>
@endsection