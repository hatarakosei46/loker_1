@extends('layout/app')

@section('title','Informasi Siswa')

@section('content')

@foreach( $users as $user)
@if($user->id == auth()->user()->id)

@if (session()->has('success_message'))
    <div class="alert alert-success text-center">
        {{ session()->get('success_message') }}
    </div>
@endif


    <main class=""uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 550; repeat: true">
      <section class="bannerCompany">
        <figure class="bannerCompany__banner">
          <img
            width="100%"
            src="/assets/img/kfcbanner.jpg"
            alt="banner-company"
          />
        </figure>

        <figure class="bannerCompany__profile">
          <img src="/uploads/avatars/{{ $user->avatar }}" alt="profile-photo-company" />
           
        </figure>   
        @foreach( $siswa as $sis)
        @if($sis->user_id == auth()->user()->id)
        <div class="box-wrap">
          <h1 class="uk-heading-small@s">{{ $sis->nama_depan }}  {{ $sis->nama_belakang }}</h1>
          <div class="box">
            <img src="/assets/img/location.svg" alt="location-logo" />
            <p>{{ $sis->alamat }}</p>
          </div>
        </div>
        @endif
        @endforeach

       <section>
        <div class="slider-profile">
            
            <button class="uk-button uk-button-default uk-margin-small-right button-tarik" type="button" uk-toggle="target: #offcanvas-slide"
            >Tarik</button>
            <div id="offcanvas-slide" uk-offcanvas="overlay: true">
                <div class="uk-offcanvas-bar"  style="background: #1e87f0;">
            
                    <div class="profil__pers uk-text-center">
                  <h2 class="uk-text-bold" style="color: white;">Dashboard</h2>
                  <img
                    class="uk-align-center"
                    src="/uploads/avatars/{{ $user->avatar }}"
                    alt=""
                  />
                  @foreach( $siswa as $sis)
                  @if($sis->user_id == auth()->user()->id)
                    <h3 class="uk-text-bold" style="color: white;">
                      {{ $sis->nama_depan }}  {{ $sis->nama_belakang }}
                    </h3>
                    <p class="uk-margin-large-bottom" style="color: white;">
                      {{ $sis->telepon }}
                    </p>
                  @endif
                  @endforeach
                </div>

                    <div class="profil__items">
                  <a href="{{ route('siswa.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon2.png"
                        alt="icon Informasi"
                      />Informasi
                    </h4></a
                  >
                  
                 <a href="{{ route('siswafavorite.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/logoprofil2.png"
                        alt=""
                      />Favorite
                    </h4></a
                  >

                  
                  <a href="{{ route('siswatawaran.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/logoprofil3.png"
                        alt=""
                      />Tawaran
                    </h4></a
                  >

                  <a href="{{ route('siswabantuan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/logoprofil4.png"
                        alt=""
                      />Bantuan
                    </h4></a
                  >


                  <a href="{{ route('terhubungperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon4.png"
                        alt="icon Terhubung"
                      />Terhubung
                    </h4></a
                  >

                  <a href="{{ route('keamananperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon5.png"
                        alt="icon Keamanan"
                      />Keamanan
                    </h4></a
                  >
                  <a href="{{ route('bantuanperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon6.png"
                        alt="icon Bantuan"
                      />Bantuan
                    </h4></a
                  >

                  <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <h4 class="uk-text-bold" style="color: white; text-align: left;">
                      <img class="uk-margin-small-right" src="/assets/img/icon7.png" alt="" />Exit
                    </h4></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>

            <div class="bantuan-siswa">
                
                <h2><strong>Bantuan<strong></h2>
                    <div class="gambar-bantuan uk-text-center">
                    <img width="100%" height="100%" src="/assets/img/imgbantuanfix.jpg" ></div>
                    <h2>Bagaimana Cara Ambil Kelas?</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                        a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>

                         @foreach( $siswa as $sis)
                  @if($sis->user_id == auth()->user()->id)
                    <h3 class="uk-text-bold" style="color: white;">
                      {{ $sis->nama_depan }}  {{ $sis->nama_belakang }}
                    </h3>
                    <p class="uk-margin-large-bottom" style="color: white;">
                      {{ $sis->telepon }}
                    </p>
                  @endif
                  @endforeach
                    
                        <div class="gambar-bantuan">
                    <img width="100%" height="100%" src="/assets/img/imgtestimoni.jpg" ></div>  
                    <h2>Cara Mengisi Testimoni</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took 
                        a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                   
 @foreach( $siswa as $sis)
                  @if($sis->user_id == auth()->user()->id)
                    <h3 class="uk-text-bold" style="color: white;">
                      {{ $sis->nama_depan }}  {{ $sis->nama_belakang }}
                    </h3>
                    <p class="uk-margin-large-bottom" style="color: white;">
                      {{ $sis->telepon }}
                    </p>
                  @endif
                  @endforeach

            </div>
            <br>
            <br>
            <br>

       </section>
      
    </main>

    @endif
    @endforeach
    @endsection

    @section('footer')
    <footer>
      <div class="flex-footer">
        <ul class="ul-list">
          <li class="header">Perusahaan</li>
          <li><a href="#"> Beranda</a></li>
          <li><a href="#">Tentang Perusahaan</a></li>
        </ul>
        <ul>
          <li class="header">Bantuan</li>
          <li><a href="#">Hubungi Kami</a></li>
          <li><a href="#">FAQ</a></li>
        </ul>
        <ul>
          <li class="header">Produk & Layanan</li>
          <li><a href="#">Lowongan Kerja</a></li>
          <li><a href="#">Pembelajaran</a></li>
          <li><a href="#"> Magang</a></li>
        </ul>
        <ul>
          <li class="header">Informasi Lainya</li>
          <li><a href="#">Testimoni</a></li>
        </ul>
      </div>

      <div class="flex-footer-2">
        <ul class="special">
          <li class="header">Temukan kami di</li>
          <div class="social">
            <a href="#"
              ><img
                class="social__img--special"
                src="/assets/img/email-2.png"
                alt="logo email"
            /></a>
            <a href="#"
              ><img
                class="social__img"
                src="/assets/img/facebook.svg"
                alt="logo facebook"
            /></a>
            <a href="#"
              ><img
                class="social__img"
                src="/assets/img/twitter.svg"
                alt="logo twitter"
            /></a>
          </div>
          <li><small>2020 - Lowongan Kerja</small></li>
          <li><small>&copy All Rights Reserved.</small></li>
        </ul>
      </div>
    </footer>

    <script src="{!! asset('/assets/js/crs.min.js') !!}"></script>
@endsection