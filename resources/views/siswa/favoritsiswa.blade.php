@extends('layout/app')

@section('title',' Dashboard Perusahaan')

@section('content')

@foreach( $users as $user)
@if($user->id == auth()->user()->id)

@if (session()->has('success_message'))
    <div class="alert alert-success text-center">
        {{ session()->get('success_message') }}
    </div>
@endif

<main class=""uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 550; repeat: true">
      <section class="bannerCompany">
        <figure class="bannerCompany__banner">
          <img
            width="100%"
            src="/assets/img/kfcbanner.jpg"
            alt="banner-company"
          />
        </figure>
        <figure class="bannerCompany__profile">
          <img src="/uploads/avatars/{{ $user->avatar }}" alt="profile-photo-company" />
        </figure>
        @foreach( $siswa as $sis)
        @if($sis->user_id == auth()->user()->id)
        <div class="box-wrap">
          <h1 class="uk-heading-small@s">{{ $sis->nama_depan }}  {{ $sis->nama_belakang }}</h1>
          <div class="box">
            <img src="/assets/img/location.svg" alt="location-logo" />
            <p>{{ $sis->alamat }}</p>
          </div>
        </div>
        @endif
        @endforeach

        <div class="uk-margin-medium-left">
                       <a href="profilsiswa-biodata.html"> <button class="uk-button uk-button-text uk-margin-medium-bottom">
                            <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#1.png" alt="">Information</h4> 
                        </button></a>
                        <br>
                        <a href="profilsiswa-fav.html">
                   <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#2.png" alt="">Favorite</h4></button></a>
                    <a href="profilsiswa-information.html">
                     <button class="uk-button uk-button-text uk-margin-medium-bottom">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#3.png" alt="">Pasang Tawaran</h4></button></a>
                    <a href="profilsiswa-bantuan.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#4.png" alt="">Bantuan</h4></button></a>

                    <br>
                    <br>
                    <br>
                    
                    <a href="index.html">
                     <button class="uk-button uk-button-text">
                    <h4 class="uk-text-bold" style="color: white;text-align: left"><img class="uk-margin-small-right" src="assets/img/logoprofil#5.png" alt="">Exit</h4></button></a>
                  

                </div>

      </section>

      <section>
        <section>
                <div class="slider-profile">
                  <button
                    class="uk-button uk-button-default uk-margin-small-right button-tarik"
                    type="button"
                    uk-toggle="target: #offcanvas-slide"
                  >
                    Tarik
                  </button>
      
                  <div id="offcanvas-slide" uk-offcanvas="overlay: true">
              <div class="uk-offcanvas-bar" style="background: #1e87f0;">
                <button
                  class="uk-offcanvas-close"
                  type="button"
                  uk-close

                ></button>
                <div class="profil__pers uk-text-center">
                  <h2 class="uk-text-bold" style="color: white;">Dashboard</h2>
                  <img
                    class="uk-align-center"
                    src="/uploads/avatars/{{ $user->avatar }}"
                    alt=""
                  />
                  @foreach( $siswa as $sis)
                  @if($sis->user_id == auth()->user()->id)
                    <h3 class="uk-text-bold" style="color: white;">
                      {{ $sis->nama_depan }}  {{ $sis->nama_belakang }}
                    </h3>
                    <p class="uk-margin-large-bottom" style="color: white;">
                      {{ $sis->telepon }}
                    </p>
                  @endif
                  @endforeach
                </div>
                <div class="profil__items">
                  <a href="{{ route('siswa.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon2.png"
                        alt="icon Informasi"
                      />Informasi
                    </h4></a
                  >
                  
                 <a href="{{ route('siswafavorite.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/logoprofil2.png"
                        alt=""
                      />Favorite
                    </h4></a
                  >

                  
                  <a href="{{ route('siswatawaran.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/logoprofil3.png"
                        alt=""
                      />Tawaran
                    </h4></a
                  >

                  <a href="{{ route('siswabantuan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/logoprofil4.png"
                        alt=""
                      />Bantuan
                    </h4></a
                  >

                  <a href="{{ route('terhubungperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon4.png"
                        alt="icon Terhubung"
                      />Terhubung
                    </h4></a
                  >

                  <a href="{{ route('keamananperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon5.png"
                        alt="icon Keamanan"
                      />Keamanan
                    </h4></a
                  >
                  <a href="{{ route('bantuanperusahaan.index') }}">
                    <h4
                      class="uk-text-bold"
                      style="color: white; text-align: left;"
                    >
                      <img
                        class="uk-margin-small-right"
                        src="/assets/img/icon6.png"
                        alt="icon Bantuan"
                      />Bantuan
                    </h4></a
                  >

                  <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <h4 class="uk-text-bold" style="color: white; text-align: left;">
                      <img class="uk-margin-small-right" src="/assets/img/icon7.png" alt="" />Exit
                    </h4></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
              </div>
            </div>

                </div>
              </section>

        <section>
                <h2 class="judul-fav" >Favorite</h2>
                <div class="cardprofilesiswa" style="padding: 4rem;">
                    
                    
                        <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid-match " uk-grid>
                            <!-- card 1 -->
                            <div>
                                <div class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center">
                                    <img src="/assets/img/MCD.svg">
                                        <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                                        <hr style="margin-top:-10px;">
                                        <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                                        <p class="uk-text-small">(Dicari seorang programmer handal yang dapat mengubah css dan html versi 5)</p>
                                        <p>
                                            <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                                            <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                                        </p>
                                </div>
                            </div>
                            <!-- end card 1 -->
                           
                            <div>
                                <div class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center">
                                    <img src="/assets/img/MCD.svg">
                                        <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                                        <hr style="margin-top:-10px;">
                                        <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                                        <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                                        <p>
                                            <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                                            <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                                        </p>
                                </div>
                            </div>

                            <div>
                                <div class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center">
                                    <img src="/assets/img/MCD.svg">
                                        <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                                        <hr style="margin-top:-10px;">
                                        <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                                        <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                                        <p>
                                            <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                                            <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                                        </p>
                                </div>
                            </div>

                            <div>
                                <div class="uk-card uk-card-default uk-card-hover uk-card-body uk-text-center">
                                    <img src="/assets/img/MCD.svg">
                                        <h4 style="margin-top: 15px;"><strong>McDonalds</strong></h4>
                                        <hr style="margin-top:-10px;">
                                        <p class="uk-text-danger"style="margin-top:-10px;" >Marketing</p>
                                        <p class="uk-text-small">(Deskripsi Lowongan Kerja)</p>
                                        <p>
                                            <button class="uk-button uk-button-danger uk-button-small">Lamar</button>
                                            <button class="uk-button uk-button-primary uk-button-small">Lihat</button>
                                        </p>
                                </div>
                            </div>
                            
                 </div>
                 <div class="button-tengah uk-text-center">
                 <button class="uk-button uk-button-primary uk-button-small " style="margin: 2rem 0 0 3rem;
                 text-transform: initial;">Lihat Semua</button>
                 </div>
        </section>
        </div>

        <div id="daftarberhasil" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <button class="uk-modal-close-default" type="button" uk-close></button>
                        <div class="uk-text-center">
                        <img  src="/assets/img/checklist.svg" alt="">
                        <h4>Terima Kasih Telah Berpartisipasi,</h4>
                        <h4>Klik dibawah ini untuk melihat postingan Anda</h4>
                        <a href=""><h4>Disini</h4></a>
                    </div>

                    
            </div>
        </div>

      </section>
    </main>

@endif
@endforeach
@endsection
    

@section('footer')
    <footer>
      <div class="flex-footer">
        <ul class="ul-list">
          <li class="header">Perusahaan</li>
          <li><a href="#"> Beranda</a></li>
          <li><a href="#">Tentang Perusahaan</a></li>
        </ul>
        <ul>
          <li class="header">Bantuan</li>
          <li><a href="#">Hubungi Kami</a></li>
          <li><a href="#">FAQ</a></li>
        </ul>
        <ul>
          <li class="header">Produk & Layanan</li>
          <li><a href="#">Lowongan Kerja</a></li>
          <li><a href="#">Pembelajaran</a></li>
          <li><a href="#"> Magang</a></li>
        </ul>
        <ul>
          <li class="header">Informasi Lainya</li>
          <li><a href="#">Testimoni</a></li>
        </ul>
      </div>

      <div class="flex-footer-2">
        <ul class="special">
          <li class="header">Temukan kami di</li>
          <div class="social">
            <a href="#"
              ><img
                class="social__img--special"
                src="/assets/img/email-2.png"
                alt="logo email"
            /></a>
            <a href="#"
              ><img
                class="social__img"
                src="/assets/img/facebook.svg"
                alt="logo facebook"
            /></a>
            <a href="#"
              ><img
                class="social__img"
                src="/assets/img/twitter.svg"
                alt="logo twitter"
            /></a>
          </div>
          <li><small>2020 - Lowongan Kerja</small></li>
          <li><small>&copy All Rights Reserved.</small></li>
        </ul>
      </div>
    </footer>

    <script src="{!! asset('/assets/js/crs.min.js') !!}"></script>
@endsection