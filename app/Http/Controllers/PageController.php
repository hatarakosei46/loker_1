<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Perusahaan;
use App\Siswa;
use App\BuatMagang;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        $siswa = Siswa::inRandomOrder()->take(2)->get();
        $perusahaan = Perusahaan::inRandomOrder()->take(5)->get();
        $buatmagang = BuatMagang::inRandomOrder()->take(5)->get();
        return view('landing', compact('users', 'perusahaan', 'siswa', 'buatmagang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pencarian(Request $request)
    {
        if($request->ajax())
        {
            $query = $request->pencarian;
            $output = "";
            $output2 = "";
            
            $data = User::get();
            $siswa = Siswa::where('nama_depan', 'like', '%'.$query.'%')
                            ->orWhere('nama_belakang', 'like', '%'.$query.'%')
                            ->orWhere('email', 'like', '%'.$query.'%')->get();
            $perusahaan = Perusahaan::where('direktur', 'like', '%'.$query.'%')
                            ->orWhere('deskripsi', 'like', '%'.$query.'%')
                            ->orWhere('email', 'like', '%'.$query.'%')->get();
            $cont1 = $siswa->count();
            $cont2 = $perusahaan->count();
            if($cont1 > 0 || $cont2 > 0)
            {

                foreach($data as $key => $dat){ 
                    foreach($siswa as $keys => $siwa){
                        if( $dat->id == $siwa->user_id){
                            $output.='<a href="#" class="card__items">'.
                                    '<figure>'.
                                    '<img src="uploads/avatars/'.$dat->avatar.'" alt="Profile-Photo-Worker">'.
                                    '</figure>'.

                                    '<div class="caption">'.
                                    '<ul>'.
                                        '<li> Nama : '.$siwa->nama_depan.'</li>'.
                                        '<li> Email : '.$siwa->email.'</li>'.
                                        '<li> Job :'.$siwa->pekerjaan_diinginkan.'</li>'.
                                        '<button class=" uk-button uk-button-primary">Detail</button>'.
                                    '</ul>'.
                                    '</div>'.
                                    '</a>';
                        }
                    }
                    foreach($perusahaan as $keyss => $peru){
                        if($dat->id == $peru->user_id){
                            $output.= '<a href="#" class="card__items">'.
                                    '<figure>'.
                                    '<img src="uploads/avatars/'.$dat->avatar.'" alt="Profile-Photo-Worker">'.
                                    '</figure>'.

                                    '<div class="caption">'.
                                    '<ul>'.
                                        '<li> Nama : '.$peru->direktur.'</li>'.
                                        '<li> Deskripsi : '.$peru->deskripsi.'</li>'.
                                        '<li> Email :'.$peru->email.'</li>'.
                                        '<button class=" uk-button uk-button-primary">Detail</button>'.
                                    '</ul>'.
                                    '</div>'.
                                    '</a>';
                        }
                    }
                }
                return Response($output);
            }else{
                $output = '
                        <div class="width-1-2@s" >
                            <ul class="uk-nav uk-dropdown-nav uk-text-left">
                                <li> Data Tidak Ditemukan </li>
                            </ul>
                        </div';
                return Response($output);
            }
        }
    }
}
