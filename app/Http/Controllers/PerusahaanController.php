<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use Image;
use App\Perusahaan;
use App\User;
use App\BuatMagang;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perusahaan = Perusahaan::get();
        $user = Auth::user();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $stat = Auth::user()->status;
            if($stat == 'siswa'){
                abort(403,'Anda tidak punya akses ke halaman ini');
            }
            return view('perusahaan.dashboard', compact('users', 'perusahaan'));
        }
    }
    public function indexMagang()
    {
        $perusahaan = Perusahaan::get();
        $user = Auth::user();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $buatmagang = BuatMagang::get();
            $stat = Auth::user()->status;
            if($stat == 'siswa'){
                abort(403,'Anda tidak punya akses ke halaman ini');
            }
            return view('perusahaan.buatmagang',  compact('users', 'perusahaan', 'buatmagang'));
        }
    }
    public function indexKeamanan()
    {
        $perusahaan = Perusahaan::get();
        $user = Auth::user();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $stat = Auth::user()->status;
            if($stat == 'siswa'){
                abort(403,'Anda tidak punya akses ke halaman ini');
            }
            return view('perusahaan.keamanan',  compact('users'));
        }
    }
    public function indexBantuan()
    {
        $perusahaan = Perusahaan::get();
        $user = Auth::user();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $stat = Auth::user()->status;
            if($stat == 'siswa'){
                abort(403,'Anda tidak punya akses ke halaman ini');
            }
            return view('perusahaan.bantuan', compact('users', 'perusahaan'));
        }
    }
    public function indexTerhubung()
    {
        $perusahaan = Perusahaan::get();
        $user = Auth::user();
        if( $user == null ){
            abort(403,'Anda tidak punya akses ke halaman ini silahkan login');
        }else{
            $users = User::where('id', auth()->user()->id)->get();
            $stat = Auth::user()->status;
            if($stat == 'siswa'){
                abort(403,'Anda tidak punya akses ke halaman ini');
            }
            return view('perusahaan.terhubung', compact('users', 'perusahaan'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user()->id;
        $perusahaan = Perusahaan::where('user_id', $user)->get('user_id')->first();
        
        if(  $perusahaan['user_id'] != $user ){
            $perusahaan = Perusahaan::create([
                'user_id' => Auth::user()->id
            ]);
            return redirect()->route('perusahaan.index');
        }else{
            return redirect()->route('perusahaan.index');
        }
    }

    public function storeMagang(Request $request)
    {        
        $perusahaan = Perusahaan::where('user_id', auth()->user()->id)->get('id')->first();
        $buatmagang = BuatMagang::where('id_perusahaan', $perusahaan['id'])->get('id_perusahaan')->first();
        
        if(  $buatmagang['id_perusahaan'] != $perusahaan['id'] ){
            $buatmagang = BuatMagang::create([
                'id_perusahaan' => $perusahaan['id']
            ]);
            return redirect()->route('buatmagangperusahaan.index');
        }else{
            return redirect()->route('buatmagangperusahaan.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function storeKeamanan(Request $request)
    {
        $usepas = Auth::user()->password;
        $lampas = $request->passwordlama;
        $barpas = $request->passwordbaru;
        if( password_verify($lampas, $usepas) ){
            User::where('id', auth()->user()->id)->update([
                'password' => Hash::make($barpas),
            ]);
            return redirect()->route('keamananperusahaan.index')->with('success_message', 'password berhasil diperbarui');
        }else{
            return redirect()->route('keamananperusahaan.index')->with('success_message', 'Password yang anda masukkan salah');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Perusahaan $perusahaan)
    {
        Perusahaan::where('user_id', auth()->user()->id)->update([
            'deskripsi' => $request->deskripsi
        ]);
        return redirect()->route('perusahaan.index')->with('success_message', 'Deskripsi berhasil diperbarui');
    }

    public function updateMagang(Request $request, BuatMagang $magang)
    {  
        $usepas = Auth::user()->password;
        $konpas = $request->password;
        $user = auth()->user()->id;
        $perusahaan = Perusahaan::where('user_id', $user)->get('id')->first();
        $perusahaans = Perusahaan::where('user_id', $user)->get('direktur')->first();
        if( password_verify($konpas, $usepas) ){

            if($request->hasFile('imagemagang')){
                $files3= $request->file('imagemagang');
                
                $ns=0;
                    $filename3 = $files3->getClientOriginalName();
                    $foldername3 = $perusahaans['direktur'];
                    $NameArr3 = $filename3;
                    $files3->move('uploads/magang/'. $foldername3, $filename3);

                    BuatMagang::where('id_perusahaan', $perusahaan['id'])->update([
                        'foto' => $NameArr3,
                    ]);
            }

            BuatMagang::where('id_perusahaan', $perusahaan['id'])->update([
                'nama_magang' => $request->jobmagang,
                'tentang_magang' => $request->tentangmagang,
                'waktu_kerja' => $request->waktukerja,
                'gaji_min' => $request->gajimin,
                'gaji_max' => $request->gajimax,
                'negara' => $request->negara,
                'kota' => $request->kota,
                'pendidikan' => $request->tinkatpendidikan,
            ]);
            return redirect()->route('buatmagangperusahaan.index')->with('success_message', 'Deskripsi berhasil diperbarui');
        }else{
            return redirect()->route('buatmagangperusahaan.index')->with('success_message', 'Password yang anda masukkan salah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function update_avatar(Request $request)
    {
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(500, 500)->save( public_path('/uploads/avatars/' . $filename) );

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        $users = User::where('id', auth()->user()->id)->get();
        $perusahaan = Perusahaan::get();

        return redirect()->route('perusahaan.index', compact('users', 'perusahaan'));

    }
}
