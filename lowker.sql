-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 07, 2020 at 05:32 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lowker`
--

-- --------------------------------------------------------

--
-- Table structure for table `buat_magangs`
--

CREATE TABLE `buat_magangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perusahaan` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_magang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tentang_magang` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waktu_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaji_min` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaji_max` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `negara` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buat_magangs`
--

INSERT INTO `buat_magangs` (`id`, `id_perusahaan`, `status`, `nama_magang`, `tentang_magang`, `waktu_kerja`, `gaji_min`, `gaji_max`, `negara`, `kota`, `pendidikan`, `jumlah`, `foto`, `created_at`, `updated_at`) VALUES
(9, 5, NULL, 'kimaklah', 'disini kami mencari orang yang siap kerja dalam tim dan memenuhi kriteria di bawah', 'Harian', '3.000.000', '15.000.000', NULL, NULL, 'Sekolah Dasar', NULL, 'coca.png', '2020-09-04 19:53:51', '2020-09-04 22:11:37'),
(10, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-05 15:11:30', '2020-09-05 15:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_sciences`
--

CREATE TABLE `category_sciences` (
  `id` int(10) UNSIGNED NOT NULL,
  `science_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail_universities`
--

CREATE TABLE `detail_universities` (
  `id` int(10) UNSIGNED NOT NULL,
  `university_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `magang_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurusans`
--

CREATE TABLE `jurusans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gelar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_mulai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biaya` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan_universities`
--

CREATE TABLE `jurusan_universities` (
  `id` int(10) UNSIGNED NOT NULL,
  `detail_university_id` int(10) UNSIGNED DEFAULT NULL,
  `jurusan_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `materis`
--

CREATE TABLE `materis` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `study_id` int(10) UNSIGNED DEFAULT NULL,
  `judul_video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download_video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_11_09_070729_create_perusahaans_table', 2),
(5, '2019_11_17_161924_create_buat_magangs_table', 2),
(6, '2019_11_21_045712_create_siswas_table', 3),
(7, '2019_11_21_062939_create_favorites_table', 4),
(8, '2019_11_30_142843_create_studies_table', 5),
(9, '2019_11_30_144509_create_materis_table', 6),
(10, '2019_12_17_021608_create_sciences_table', 7),
(11, '2019_12_17_022132_create_sciences_table', 8),
(12, '2019_12_17_022242_create_categories_table', 9),
(13, '2019_12_17_022855_create_category_sciences_table', 10),
(14, '2019_12_17_024004_create_universities_table', 11),
(15, '2019_12_17_024539_create_detal_universities_table', 12),
(16, '2019_12_17_024931_create_detail_universities_table', 13),
(17, '2019_12_17_025107_create_jurusans_table', 14),
(18, '2019_12_17_030038_create_jurusan_universities_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `perusahaans`
--

CREATE TABLE `perusahaans` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direktur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_lain` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_tlp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perusahaans`
--

INSERT INTO `perusahaans` (`id`, `user_id`, `deskripsi`, `direktur`, `email`, `email_lain`, `nomor_tlp`, `google`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(2, 6, 'Saya Kuliah di amiko', 'andi permana', '', '', '0', NULL, NULL, NULL, '2019-11-20 06:46:01', '2019-11-20 06:46:26'),
(4, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-07 08:15:49', '2020-05-07 08:15:49'),
(5, 11, 'Pujangga webtoon sejati', 'DilloJogja', NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-17 23:39:42', '2020-09-03 10:50:51'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-04 19:42:51', '2020-09-04 19:42:51'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-04 19:43:49', '2020-09-04 19:43:49'),
(8, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-05 15:11:24', '2020-09-05 15:11:24'),
(9, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-05 15:13:58', '2020-09-05 15:13:58'),
(10, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-05 15:30:36', '2020-09-05 15:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `sciences`
--

CREATE TABLE `sciences` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `siswas`
--

CREATE TABLE `siswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `code` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_depan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_belakang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `negara` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tinggi_badan` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan_diinginkan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siap_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kerja_di_luarnegeri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pengalaman_kerja` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penulisan_bahasainggris` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bacaan_bahasainggris` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswas`
--

INSERT INTO `siswas` (`id`, `user_id`, `code`, `nama_depan`, `nama_belakang`, `email`, `password`, `telepon`, `alamat`, `negara`, `tanggal_lahir`, `jenis_kelamin`, `tinggi_badan`, `status`, `pendidikan`, `foto`, `video`, `visa`, `pekerjaan_diinginkan`, `siap_kerja`, `kerja_di_luarnegeri`, `pengalaman_kerja`, `bahasa`, `penulisan_bahasainggris`, `bacaan_bahasainggris`, `created_at`, `updated_at`) VALUES
(7, 8, NULL, 'miochan', NULL, 'mio.akiyama909@gmail.com', '$2y$10$HdUj/ECYFJQOTWe92q0zVu/ZIF1NtOyGRO.XK.Ux1eqBSkUm53.2i', NULL, NULL, 'Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Inggris & Indonesia', NULL, NULL, '2020-01-18 08:20:12', '2020-01-18 08:20:12'),
(8, 9, NULL, 'addin', 'kalijo', 'akiyama.mio909@gmail.com', '$2y$10$/03ndhHn78YE2lfe7FkRMOuSwAsj4BNbSA7uEXK4fZwTHntuR/hsq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-27 01:25:31', '2020-09-05 21:00:01'),
(9, 15, NULL, 'addin saputra', NULL, 'adminsuper@gmail.com', '$2y$10$roc/YDMMO2ALpZ3wXv6HSeOxsfAo1fXld9gqEeYTqaNQXo34YIHwm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-05 16:45:52', '2020-09-05 16:45:52');

-- --------------------------------------------------------

--
-- Table structure for table `studies`
--

CREATE TABLE `studies` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nama_kelas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kualitas_video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(10) UNSIGNED NOT NULL,
  `science_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peringkat` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(244) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `provider`, `provider_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Barjono', 'barjono@gmail.com', 'user.png', NULL, '$2y$10$PIWQhpYkTxoc4KOltGslXeUDKKSlZZ1KPFJY6SgyH/rGWlv3XDqUe', NULL, NULL, 'siswa', NULL, '2019-10-31 03:09:02', '2019-10-31 03:09:02'),
(6, 'Arfian Amikom', 'arfian.1018@students.amikom.ac.id', 'user.png', NULL, '$2y$10$rC7yfyIb50x9R.dXmePHqOywsKrcHJNb.mlBniJY.qoEPO6VKAOfW', NULL, NULL, 'perusahaan', NULL, '2019-11-20 06:44:48', '2019-11-23 23:32:41'),
(7, 'Barjana Jana', 'barjana@gmail.com', 'user.png', NULL, '$2y$10$74dgt1nNVfZoAUqCTPvbV.9rAgG0dcfWZSZb7YqpVGG.iFmpbuvrq', NULL, NULL, 'siswa', NULL, '2019-11-20 06:47:53', '2019-11-20 08:08:33'),
(8, 'miochan', 'mio.akiyama909@gmail.com', 'user.png', NULL, '$2y$10$HdUj/ECYFJQOTWe92q0zVu/ZIF1NtOyGRO.XK.Ux1eqBSkUm53.2i', NULL, NULL, 'siswa', NULL, '2019-12-21 03:30:31', '2019-12-21 03:30:31'),
(9, 'addin', 'akiyama.mio909@gmail.com', 'user.png', NULL, '$2y$10$/03ndhHn78YE2lfe7FkRMOuSwAsj4BNbSA7uEXK4fZwTHntuR/hsq', NULL, NULL, 'siswa', NULL, '2020-03-27 01:25:27', '2020-04-06 02:51:09'),
(10, 'kotobuki', 'muhammad.1050@students.amikom.ac.id', 'user.png', NULL, '$2y$10$kA3lZHjoLcoGFlurz8gaGeUqKDJvieLgMraOUZ9e4HfLmrgaJBkAq', NULL, NULL, 'perusahaan', NULL, '2020-03-31 01:50:13', '2020-03-31 01:50:13'),
(11, 'akashi record', 'admin@ayosinau.com', '1599259827.png', NULL, '$2y$10$TpQqJikSdDBSKq0E405KBu/r3pYqIJUEbLpfFomOiCVyV3sXl3nC6', NULL, NULL, 'perusahaan', NULL, '2020-07-17 23:39:40', '2020-09-04 22:49:37'),
(12, 'TimHoreProduction', 'makino@gmail.com', 'user.png', NULL, '$2y$10$S/6cDy9Hyf4YQ5LIAnkO7ec2ijWtG1oo/sU8PGa8pXX0VNliqdcQm', NULL, NULL, 'perusahaan', NULL, '2020-09-05 15:10:44', '2020-09-05 15:10:44'),
(13, 'no service worker', 'addin.saput@gmail.com', 'user.png', NULL, '$2y$10$YDozlIO6rLZ5nPNH8kZNNeRr2yk7V5DqISEW6ext9ey/ESsaR8n7m', NULL, NULL, 'perusahaan', NULL, '2020-09-05 15:13:28', '2020-09-05 15:13:28'),
(14, 'akashi record', 'dianjogja@gmail.com', 'user.png', NULL, '$2y$10$ALMkBFuVI5AdSV81/tgg6uLujzSgdHraf7.F.Mqh/seYMTUluC9gi', NULL, NULL, 'perusahaan', NULL, '2020-09-05 15:30:32', '2020-09-05 15:30:32'),
(15, 'addin saputra', 'adminsuper@gmail.com', 'user.png', NULL, '$2y$10$roc/YDMMO2ALpZ3wXv6HSeOxsfAo1fXld9gqEeYTqaNQXo34YIHwm', NULL, NULL, 'siswa', NULL, '2020-09-05 16:44:03', '2020-09-05 16:44:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buat_magangs`
--
ALTER TABLE `buat_magangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buat_magangs_id_perusahaan_foreign` (`id_perusahaan`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_sciences`
--
ALTER TABLE `category_sciences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_sciences_science_id_foreign` (`science_id`),
  ADD KEY `category_sciences_category_id_foreign` (`category_id`);

--
-- Indexes for table `detail_universities`
--
ALTER TABLE `detail_universities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_universities_university_id_foreign` (`university_id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_user_id_foreign` (`user_id`),
  ADD KEY `favorites_magang_id_foreign` (`magang_id`);

--
-- Indexes for table `jurusans`
--
ALTER TABLE `jurusans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan_universities`
--
ALTER TABLE `jurusan_universities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurusan_universities_detail_university_id_foreign` (`detail_university_id`),
  ADD KEY `jurusan_universities_jurusan_id_foreign` (`jurusan_id`);

--
-- Indexes for table `materis`
--
ALTER TABLE `materis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materis_user_id_foreign` (`user_id`),
  ADD KEY `materis_study_id_foreign` (`study_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `perusahaans`
--
ALTER TABLE `perusahaans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perusahaans_user_id_foreign` (`user_id`);

--
-- Indexes for table `sciences`
--
ALTER TABLE `sciences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswas`
--
ALTER TABLE `siswas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswas_user_id_foreign` (`user_id`);

--
-- Indexes for table `studies`
--
ALTER TABLE `studies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studies_user_id_foreign` (`user_id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `universities_science_id_foreign` (`science_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buat_magangs`
--
ALTER TABLE `buat_magangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_sciences`
--
ALTER TABLE `category_sciences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_universities`
--
ALTER TABLE `detail_universities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurusans`
--
ALTER TABLE `jurusans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurusan_universities`
--
ALTER TABLE `jurusan_universities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materis`
--
ALTER TABLE `materis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `perusahaans`
--
ALTER TABLE `perusahaans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sciences`
--
ALTER TABLE `sciences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siswas`
--
ALTER TABLE `siswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `studies`
--
ALTER TABLE `studies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buat_magangs`
--
ALTER TABLE `buat_magangs`
  ADD CONSTRAINT `buat_magangs_id_perusahaan_foreign` FOREIGN KEY (`id_perusahaan`) REFERENCES `perusahaans` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `category_sciences`
--
ALTER TABLE `category_sciences`
  ADD CONSTRAINT `category_sciences_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_sciences_science_id_foreign` FOREIGN KEY (`science_id`) REFERENCES `sciences` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `detail_universities`
--
ALTER TABLE `detail_universities`
  ADD CONSTRAINT `detail_universities_university_id_foreign` FOREIGN KEY (`university_id`) REFERENCES `universities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_magang_id_foreign` FOREIGN KEY (`magang_id`) REFERENCES `buat_magangs` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `jurusan_universities`
--
ALTER TABLE `jurusan_universities`
  ADD CONSTRAINT `jurusan_universities_detail_university_id_foreign` FOREIGN KEY (`detail_university_id`) REFERENCES `detail_universities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jurusan_universities_jurusan_id_foreign` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `materis`
--
ALTER TABLE `materis`
  ADD CONSTRAINT `materis_study_id_foreign` FOREIGN KEY (`study_id`) REFERENCES `studies` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `materis_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `perusahaans`
--
ALTER TABLE `perusahaans`
  ADD CONSTRAINT `perusahaans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `siswas`
--
ALTER TABLE `siswas`
  ADD CONSTRAINT `siswas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `studies`
--
ALTER TABLE `studies`
  ADD CONSTRAINT `studies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `universities`
--
ALTER TABLE `universities`
  ADD CONSTRAINT `universities_science_id_foreign` FOREIGN KEY (`science_id`) REFERENCES `sciences` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
