<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@index')->name('/');
Route::get('/register','PageController@index')->name('register');
Route::get('/login','PageController@index')->name('login');
Route::get('/pencarian','PageController@pencarian')->name('pencarian.cari');
//login ROUTE
Route::post('/login2', 'Auth\LoginController@postlogin')->name('login2');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'perusahaan','middleware'=>'auth'], function(){
    // PERUSAHAAN ROUTE
    Route::get('/', 'PerusahaanController@index')->name('perusahaan.index');
    Route::post('/avatar', 'PerusahaanController@update_avatar')->name('perusahaan.avatar');
    Route::get('/store', 'PerusahaanController@store')->name('perusahaan.store');
    Route::post('/update', 'PerusahaanController@update')->name('perusahaan.update');
    Route::get('/informasi', 'InformasiController@index')->name('informasiperusahaan.index');
    Route::post('/informasi', 'InformasiController@update')->name('informasiperusahaan.update');
    Route::get('/buatmagang', 'PerusahaanController@indexMagang')->name('buatmagangperusahaan.index');
    Route::get('/buatmagang store', 'PerusahaanController@storeMagang')->name('buatmagangperusahaan.store');
    Route::post('/buatmagang', 'PerusahaanController@updateMagang')->name('buatmagangperusahaan.update');
    Route::get('/terhubung', 'PerusahaanController@indexTerhubung')->name('terhubungperusahaan.index');
    Route::get('/keamanan', 'PerusahaanController@indexKeamanan')->name('keamananperusahaan.index');
    Route::post('/keamanan', 'PerusahaanController@storeKeamanan')->name('keamananperusahaan.store');
    Route::get('/bantuan', 'PerusahaanController@indexBantuan')->name('bantuanperusahaan.index');
    // END PERUSAHAAN ROUTE
});


Route::group(['prefix'=>'siswa','middleware'=>'auth'], function(){
    // SISWA ROUTE
    Route::get('/', 'SiswaProfilController@index')->name('siswa.index');
    Route::post('/avatar', 'SiswaProfilController@update_avatar')->name('siswa.avatar');
    Route::get('/store', 'SiswaProfilController@store')->name('siswa.store');
    Route::post('/informasi', 'SiswaProfilController@update')->name('siswa.update');
    Route::get('/favorite', 'SiswaProfilController@indexFavorite')->name('siswafavorite.index');
    Route::get('/tawaran', 'SiswaProfilController@indexTawaran')->name('siswatawaran.index');
    Route::get('/bantuan', 'SiswaProfilController@indexBantuan')->name('siswabantuan.index');
    // END SISWA ROUTE
});